<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output
		method="html"
		version="1.0"
		encoding="UTF-8"
		omit-xml-declaration="yes"
		indent="yes"
		doctype-system="about:legacy-compat"
		/>

	<xsl:template match="/">
		<html lang="en">
			<head>
				<title><xsl:apply-templates select="article/header/h1" mode="text"/> - PerIta Soft</title>
				<meta name="viewport" content="width=device-width, initial-scale=1"/>
				<meta name="description">
					<xsl:attribute name="content">
						<xsl:value-of select="/article/@description"/>
					</xsl:attribute>
				</meta>
				<link rel="stylesheet" type="text/css" media="screen" href="../screen.css" />
			</head>
			<body>
				<header>
					<h1>
						<img src="../perita.png" alt="" />
						Perita’s Blog
					</h1>
					<p><a href="../index.html" rel="index">← Return to blog index</a></p>
				</header>
				<xsl:apply-templates />
			</body>
		</html>
	</xsl:template>

	<xsl:template match="article">
		<article itemscope="itemscope" itemtype="http://schema.org/BlogPosting" class="h-entry">
			<xsl:apply-templates select="header"/>
			<div itemprop="articleBody" class="e-content">
				<xsl:apply-templates select="*[position() &gt; 1]"/>
			</div>
			<a href="/" class="u-author" aria-hidden="true"></a>
			<footer>
				<p><a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" src="../cc-by-sa-4.0-88x31.png" width="88" height="31"/></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.</p>
			</footer>
		</article>
	</xsl:template>

	<xsl:template match="h1">
		<h2>
			<xsl:attribute name="itemprop">headline</xsl:attribute>
			<xsl:apply-templates select="@*|node()"/>
		</h2>
	</xsl:template>

	<xsl:template match="time[@class='dt-published']">
		<xsl:copy>
			<xsl:attribute name="itemprop">datePublished</xsl:attribute>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>
