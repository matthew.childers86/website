<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:date="http://exslt.org/dates-and-times"
	extension-element-prefixes="date"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output
		method="html"
		version="1.0"
		encoding="UTF-8"
		omit-xml-declaration="yes"
		indent="yes"
		doctype-system="about:legacy-compat"
		/>

	<xsl:template match="/">
		<html lang="en">
			<head>
				<title>Blog Post Index by Date - PerIta Soft</title>
				<meta name="viewport" content="width=device-width, initial-scale=1"/>
				<meta name="description" content="List of all articles we write about the development of our games, what problems we find along the way, and how we solve them."/>
				<link rel="stylesheet" type="text/css" media="screen" href="screen.css" />
			</head>
			<body>
				<header>
					<h1>
						<img src="perita.png" alt="" />
						Perita’s Blog
					</h1>
					<p><a href="../index.html" rel="home">← Return to main page</a></p>
				</header>
				<article class="h-feed">
					<header>
						<h2 class="p-name">Post Index by Date</h2>
					</header>
					<xsl:apply-templates />
					<footer>
						<p>Last updated on <time>
								<xsl:attribute name="datetime"><xsl:value-of select="date:date-time()"/></xsl:attribute>
								<xsl:value-of select="date:day-in-month()"/>
								<xsl:text> </xsl:text>
								<xsl:value-of select="date:month-name()"/>
								<xsl:text> </xsl:text>
								<xsl:value-of select="date:year()"/>
						</time>.</p>
					</footer>
				</article>
			</body>
		</html>
	</xsl:template>

	<xsl:template match="posts">
		<ol id="posts">
			<xsl:apply-templates/>
		</ol>
	</xsl:template>

	<xsl:template match="post">
		<li class="h-entry">
			<p>
				<a class="p-name u-url">
					<xsl:attribute name="href">
						<xsl:value-of select="@href"/>
					</xsl:attribute>
					<xsl:apply-templates select="document(@src)/article/header/h1"/>
				</a>
			</p>
			<p class="p-summary"><xsl:apply-templates select="document(@src)/article/@description"/></p>
			<p><xsl:copy-of select="document(@src)//time[@class='dt-published']"/></p>
		</li>
	</xsl:template>


</xsl:stylesheet>
