<?xml version="1.0" encoding="UTF-8"?>
<article description="We introduce Flick Karts, a new video game written in Java for Android, desktop, and the web inspired by the board game Pitch Car.">

<header>
	<h1 class="p-name"><span class="h-product p-name">Flick Karts</span>: A New Beginning</h1>
	<p><time class="dt-published" datetime="2020-08-23">23 August 2020</time></p>
</header>

<p>
We started writting a new game this week:
<a href="https://www.peritasoft.com/flickkarts/" class="h-product p-name">Flick Karts</a>, a digital implementation of the board game <a href="https://www.ferti-games.com/enpitchcar" class="h-product p-name">PitchCar</a>, a racing simulation where players flick small pucks around a track constructed with puzzle-like pieces, but extended with features hard to do in a physical game, such as different weather conditions and power-up items.
In this version the pucks represent karts and players set the angle and force of the flicking by pulling back from the puck’s center, similar to a slingshot.
</p>

<p>
After our <a href="https://gitlab.com/perita/tramposillu">foray into C# and MonoGame</a>, we decided that it was not our cup of tea and went back to Java and <a href="https://libgdx.badlogicgames.com/" class="h-product p-name">libGDX</a>.
The first order of business was to set <a href="https://box2d.org/" class="h-product p-name">Box2D</a>’s <code>World</code> up so that it has no gravity
(i.e., it is a top-down view)
and can flick the puck around a test track with the mouse.
</p>

<figure style="max-width: 800px">
	<video controls="controls">
		<source src="gameplay.webm" type="video/webm"/>
	</video>
	<figcaption>Flicking the puck around with the mouse.</figcaption>
</figure>

<p>
It seems, however, that we are easily confused with what units to use in <span class="h-product p-name">Box2D</span> and how they map to screen coordinates.
Our first attempt was to use centimeters to define the puck’s size and having a view of the world of 3&#8201;×&#8201;1,5&#160;meters.
</p>

<figure>
<pre><code>
…
CircleShape shape = new CircleShape();
shape.setRadius(3f);
…
viewport = new ExtendViewport(300f, 150f);
…
</code></pre>
</figure>

<p>
The problem is that <span class="h-product p-name">Box2D</span> then assumes that the puck is a six-meter circle and it was very hard to flick it with the necessary impulse to make it go fast.
Therefore, we ended up using meters for everything.
</p>

<figure>
<pre><code>
…
CircleShape shape = new CircleShape();
shape.setRadius(0.3f);
…
viewport = new ExtendViewport(3f, 1.5f);
…
</code></pre>
</figure>

</article>
